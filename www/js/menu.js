window.onscroll = function () { scrollMenu() };

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    //document.getElementById("main").style.marginLeft = "250px";
    document.getElementById("main").style.opacity = 0;
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    //document.getElementById("main").style.marginLeft = "0";
    document.getElementById("main").style.opacity = 1;
    document.body.style.backgroundColor = "white";
}



function scrollMenu() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}